// const someCommonValues = ['common', 'values'];

const utils = {
  cssSafeStr(str){
    return encodeURIComponent(str)
    .toLowerCase()
    .replace(/\.|%[0-9a-z]{2}/gi, '')
    .replace(/_/g, '-');
  },

  camelizeStr(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
  },

  capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },

  classSafeStr(str){
    return encodeURIComponent(utils.cssSafeStr(str)).split('-').map((strItem)=>{
      return utils.capitalizeFirstLetter(strItem)
    }).join("")
  },
}

export default utils