import { NavLink } from 'react-router-dom'

const Footer = () => {
    return (
        <div id="footer">
            <div className="container text-center">
                <div className="col-xs-12 col-md-6">
                    <p>Oliveira Kitchen &copy;2021 | Designed by <a href="http://www.entwicklungs-umgebung.de" rel="nofollow">MK</a> | <span className="sub-nav"><NavLink to="/">Home</NavLink> | <NavLink to="/impressum">Impressum</NavLink> | <NavLink to="/datenschutz">Datenschutz</NavLink></span></p>
                </div>
                <div className="col-xs-12 col-md-6">
                    <div className="social">
                        <ul>
                            <li><a href="#fb" ><i className="fa fa-facebook"></i></a></li>
                            <li><a href="#tw" ><i className="fa fa-twitter"></i></a></li>
                            <li><a href="#yt" ><i className="fa fa-youtube"></i></a></li>
                            <li><a href="https://oliveira-kitchen.prismic.io/documents/working?l=de-de" target="_blank"  className="ico-prismic"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer