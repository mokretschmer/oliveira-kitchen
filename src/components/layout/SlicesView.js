import React, {useEffect, useState} from 'react'
import utils from 'utils/utils'

import Slices from 'components/slices'


const SlicesView = (props) => {

    const [sliceOutput, setSliceOutput] = useState('')
    
    useEffect(() => {
        if(props.data){
            const slicesHtml = props.data.map((slice, index) => {
                //dynamic import of module
                const moduleName = utils.classSafeStr(slice.slice_type)
                if(Slices.hasOwnProperty(moduleName)){
                    let Component = Slices[moduleName];
                    slice.docData = props.docData
                    slice.index = index
                      return <Component key={moduleName} sliceData={slice} />
                }
                else{
                    console.warn(`Component "${moduleName}" was not found.`)
                }
                return null
              }
            )
            setSliceOutput(slicesHtml)
        }
    }, [props.data, props.docData])

    return (
        <section className="slices">
            {sliceOutput}
        </section>
    )
}

export default SlicesView