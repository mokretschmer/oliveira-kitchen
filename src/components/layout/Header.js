const Header = () => {
    return (
        <section className="ui-ele ui-ele--header">
            <header>
                <div className="intro" style={{ 'background-image': `url( ${process.env.PUBLIC_URL + '/img/intro-bg.jpg'})`, }}>
                    <div className="overlay">
                        <div className="container">
                            <div className="row">
                                <div className="intro-text">
                                    <h1 className="title">Oliveira<span>Kitchen</span></h1>
                                    <p className="subline">Rufen Sie uns an:<br /><i>0152 227 120 58</i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </section>
    )
}

export default Header