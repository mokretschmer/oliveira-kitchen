import React, { useContext, useRef } from 'react'
import { EventContext } from 'store/EventContext'
import { useListener } from 'react-bus'
import Form from 'lib/Form'
import { Date } from 'prismic-reactjs'

/**
 * @config preparing config prop.
 * api: url for the server endpoint
 * title: form title
 * successMessage: message will show in the UI when mail is successfully sent.
 * errorMessage: message will show in the UI when mail is not sent.
 * fieldsConfig = settings for each input/textarea field
 */




const ContactForm = (props) => {

  const { events } = useContext(EventContext)
  const uiSelect = useRef()
  const formRef = useRef()

  const subjectOptions = () => {
    const subjectEventOptions = events.map((event, index) => {
      const dateOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
      const eventDate = Date(event.data.event_date).toLocaleDateString('de-DE', dateOptions)
      return <option key={index} value={event.id}>Event: {event.data.title[0].text} ({eventDate})</option>
    })

    return (
      <>
        <option value="Anfrage">Anfrage</option>
        {subjectEventOptions}
      </>
    )
  }


  const uiSelectSetInitialValue = React.useCallback(obj => {
    
    //reset form
    formRef.current.resetForm()

    //check if ui control has option
    setTimeout(()=>{
      uiSelect.current.value = obj.event
    }, 100)


  }, [uiSelect])
  useListener('SCROLL_TO_FORM', uiSelectSetInitialValue)


  const config = {
    api: `${process.env.NODE_ENV === "development" ? process.env.REACT_APP_CONTACT_FORM_API_URL__DEV : process.env.REACT_APP_CONTACT_FORM_API_URL__PRODUCTION}`,
    title: 'Kontaktform',
    description: 'Kontaktanfrage oder Event',
    errorMessage: 'Formular nicht vollständig ausgefüllt.',
    successMessage: <>Danke für deine Nachricht.<br/>Wir melden uns!</>,
    formActionsWrapperClasses: 'col-md-12',
    formMessageWrapperClasses: 'col-md-12',
    fields: [
      { id: 1, label: 'Subject', labelShown: false, fieldName: 'subject', type: 'select', placeholder: '', isRequired: false, classes: 'form-control', wrapperClasses: 'col-md-6', ref: uiSelect, options: subjectOptions()},
      { id: 2, label: 'Name', labelShown: false, fieldName: 'name', type: 'text', placeholder: 'Name', isRequired: true, classes: 'form-control', wrapperClasses: 'col-md-6', },
      { id: 3, label: 'Email', labelShown: false, fieldName: 'email', type: 'email', placeholder: 'E-Mail', isRequired: true, classes: 'form-control', wrapperClasses: 'col-md-6', },
      { id: 4, label: 'Tel', labelShown: false, fieldName: 'tel', type: 'text', placeholder: 'Tel', isRequired: false, classes: 'form-control', wrapperClasses: 'col-md-6', },
      { id: 5, label: 'Message', labelShown: false, fieldName: 'msg', type: 'textarea', placeholder: 'Ihre Nachricht...', isRequired: true, classes: 'form-control', wrapperClasses: 'col-md-12', }
    ]
  }

  return (
    <div className="container form-wrapper">
      <div className="section-title text-center">
        <h2>Anfrage</h2>
        <h3 className="subline">Gerne rufen wir dich zurück</h3>
      </div>
      <div className="col-md-8 col-md-offset-2">
        <Form ref={formRef} config={config}></Form>
      </div>
    </div>
  )
}

export default ContactForm

