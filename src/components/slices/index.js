import Header from './Header'
import Slice from 'lib/Slice'
import Services from './Services'
import Menu from './Menu'
import StoryBlock from './StoryBlock'
import Gallery from './Gallery'
import Contact from './Contact'
import About from './About'

const Slices = {
  Slice,
  Header,
  Services,
  Menu,
  StoryBlock,
  Gallery,
  Contact,
  About,
}

export default Slices