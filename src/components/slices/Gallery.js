import React from 'react'
import Slice from 'lib/Slice';

const Gallery = (props) => {

  const galleryItemsOutput = props.sliceData.items.map((item, index) => {

    return (
      <div key={index} className="col-xs-6 col-md-3">
        <div className="gallery-item ">
          <img className="img-responsive" alt={item.image.alt} src={item.image.url} />
        </div>
      </div>
    )
  })

  return (
    <Slice {...props}>
      <div className="container-fluid">
        <div className="row">

          {galleryItemsOutput}

        </div>
      </div>
    </Slice>
  )
}

export default Gallery