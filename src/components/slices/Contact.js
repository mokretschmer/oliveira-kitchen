import React from 'react'
import { RichText } from 'prismic-reactjs'

import Slice from 'lib/Slice';
import ContactForm from 'components/forms/ContactForm'


const Contact = (props) => {

  
  return (
    <Slice {...props}>
      <div className="container text-center info-wrapper">
        <div className="col-md-4">
          <RichText render={props.sliceData.primary.block_1_title} />
          <div className="contact-item ">
            <RichText render={props.sliceData.primary.block_1_text} />
          </div>
        </div>
        <div className="col-md-4">
          <RichText render={props.sliceData.primary.block_2_title} />
          <div className="contact-item ">
            <RichText render={props.sliceData.primary.block_2_text} />
          </div>
        </div>
        <div className="col-md-4">
          <RichText render={props.sliceData.primary.block_3_title} />
          <div className="contact-item ">
            <RichText render={props.sliceData.primary.block_3_text} />
          </div>
        </div>
      </div>

      <ContactForm />

    </Slice>
  )
}

export default Contact
