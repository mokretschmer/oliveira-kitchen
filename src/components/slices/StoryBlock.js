import React from 'react'
import Slice from 'lib/Slice';
import { RichText } from 'prismic-reactjs'

const StoryBlock = (props) => {

  return (
      <Slice {...props}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12 col-md-6 about-img" style={{backgroundImage:`url( ${props.sliceData.primary.image.url})`,}}></div>
            <div className="col-xs-12 col-md-3 col-md-offset-1">
              <div className="about-text">
                <div className="section-title">
                  <RichText render={props.sliceData.primary.titel} />
                </div>
                <RichText render={props.sliceData.primary.text} />
              </div>
            </div>
          </div>
        </div>
      </Slice>
  )
}

export default StoryBlock