import React from 'react'
import { RichText } from 'prismic-reactjs'
import Slice from 'lib/Slice';

const Header = (props) => {

  const backgroundImageUrl = props.sliceData.primary.header_image

  return (
      <Slice {...props}>
        <header>
          <div className="intro" style={{backgroundImage: `url(${backgroundImageUrl.url})`}}>
            <div className="overlay">
              <div className="container">
                <div className="row">
                  <div className="intro-text">
                    <h1>Oliveira<span>Kitchen</span></h1>
                    <p>{RichText.asText(props.sliceData.primary.header_subline)}<br /><i style={{fontWeight: 'bold', fontStyle: 'normal',}}>{ RichText.asText(props.sliceData.primary.header_telephone) }</i></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      </Slice>
  )
}

export default Header