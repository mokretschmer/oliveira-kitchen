import { RichText } from 'prismic-reactjs'

const MenuCourse = (props) => {

  const itemsOutput = props.items.map((item, index) => {
    return (
      <div key={index} className="menu-item">
        <div className="menu-item-name ">
          <p>{ RichText.asText(item.dish_name) }</p>
        </div>
        <div className="menu-item-description ">
          <p>{ RichText.asText(item.dish_text) }</p>
        </div>
      </div>
    )
  })

  return (
    <div className="col">
      <div className="menu-section">
        <h2 className="menu-section-title">{props.title}</h2>
        {itemsOutput}
      </div>
    </div>

  )
}

export default MenuCourse