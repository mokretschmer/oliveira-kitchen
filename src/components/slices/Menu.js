import React, {useState, useEffect, useContext} from 'react'
import { RichText, Date } from 'prismic-reactjs'
import {useBus} from 'react-bus'
import Slice from 'lib/Slice';
import {EventContext} from 'store/EventContext'
import MenuCourse from './MenuCourse'


const Menu = (props) => {

  const [eventRef, setEventRef] = useState(null)
  const {events, getEntityById} = useContext(EventContext)
  const bus = useBus()

  //event ref
  useEffect(() => {
    getEntityById(props.sliceData.primary.event_ref.id, {callback: onEventLoaded})
  }, [getEntityById, props.sliceData.primary.event_ref.id])

  const onEventLoaded = (event) => {
    if(event){
      setEventRef(event)
    }
  }

  const onButtonBookEventClick = event => {
    bus.emit('SCROLL_TO_FORM', {
      event: eventRef.id
    })
  }

  let itemsOutput = ""
  let eventDate = ""
  let btnOutput = "" // lives in context

  const findObjectByKey = (array, key, value) => {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

  if(eventRef){
    const dateOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
    eventDate = Date(eventRef.data.event_date).toLocaleDateString('de-DE', dateOptions)
    itemsOutput = eventRef.data.body.map((item, index) => {
      const course_title = RichText.asText(item.primary.course_name)
      return (
        <MenuCourse key={index} title={course_title} items={item.items} />
      )
    })
    if(findObjectByKey(events, 'id', eventRef.id)){
      btnOutput = (
        <div className="row">
          <div className="event-action text-center col-xs-12">
            <button onClick={onButtonBookEventClick} type="submit" className="btn btn-custom btn-event-cta btn-xl">Eventanfrage</button>
          </div>
        </div>
      )
    }
    
  }

  return (eventRef && eventDate) && (
    <Slice {...props}>
      {/* Restaurant Menu Section  */}
      <div className="container">
        <div className="section-title text-center">
          <h2>Men&uuml;</h2>
          <h3 className="subline">Event: <b>{ RichText.asText(eventRef.data.title) }</b> ({ eventDate.toString() })</h3>
        </div>
        <div className="flex-row">
          {itemsOutput}
        </div>
        {btnOutput}
      </div>

    </Slice>
  )
}

export default Menu