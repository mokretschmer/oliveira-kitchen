import React from 'react'
import { RichText } from 'prismic-reactjs'
import Slice from 'lib/Slice';

const About = (props) => {

  const colTextWrapper = parseInt(Object.keys(props.sliceData.primary.about_image).length) ? 'col-md-7' : 'col-md-10 col-md-offset-1'
  const colText = parseInt(Object.keys(props.sliceData.primary.about_image).length) ? 'col-md-7 col-md-offset-3' : 'col-md-10 col-md-offset-1'
  console.log(Object.keys(props.sliceData.primary.about_image).length)

  return (
    <Slice {...props}>
      <div className="container">
        <div id="row">
          <div className={colTextWrapper}>
            <div className={colText}>
              <div className="section-title">
              <RichText render={props.sliceData.primary.title} />
              </div>
              <RichText render={props.sliceData.primary.text} />
            </div>
          </div>
          {props.sliceData.primary.about_image && 
          <div className="col-md-5">
            <div className="team-img ">
              <p><img alt={props.sliceData.primary.about_image.alt} src={props.sliceData.primary.about_image.url} /></p>
            </div>
          </div>
          }
        </div>
      </div>
    </Slice>
  )
}

export default About