import React from 'react'
import Slice from 'lib/Slice';
import { RichText } from 'prismic-reactjs'

const Services = (props) => {


  const itemsOutput = props.sliceData.items.map((item, index) => {
    return <div key={index} className="col-xs-12 col-sm-4">
      <div className="features-item">
        
        <img src={item.service_image.url} className="img-responsive" alt={item.service_image.alt} />
        <h3 className="title">{RichText.asText(item.service_title)}</h3>
        <RichText render={item.service_text} />
      </div>
    </div>
  })

  return (
    <Slice {...props}>
      <div className="text-center">
        <div className="container">
          <div className="section-title">
            <RichText render={props.sliceData.primary.slice_introtext} />
          </div>
          <div className="row">

            {itemsOutput}

          </div>
        </div>
      </div>
    </Slice>
  )
}

export default Services