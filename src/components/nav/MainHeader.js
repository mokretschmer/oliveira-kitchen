import { NavLink } from 'react-router-dom'
import Scrollspy from './../../assets/js/scrollspy'

const MainHeader = () => {
    return (
        <nav id="main-menu" className="navbar navbar-default navbar-fixed-top">
            <div className="container">
                {/* Brand and toggle get grouped for better mobile display  */}
                <div className="navbar-header">
                    <span className="brand-name">Oliveira Kitchen</span>
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span className="sr-only">Toggle navigation</span> <span className="icon-bar"></span> <span className="icon-bar"></span> <span className="icon-bar"></span> </button>
                </div>

                {/* Collect the nav links, forms, and other content for toggling  */}
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <Scrollspy offset={-90} className="nav navbar-nav" items={['features', 'about', 'event-menu', 'team', 'contact']} currentClassName="active" >
                        <li><NavLink to="home#services" className="page-scroll ">Services</NavLink></li>
                        <li><NavLink to="home#story-block" className="page-scroll ">Team</NavLink></li>
                        <li><NavLink to="home#menu" className="page-scroll ">Event Menü</NavLink></li>
                        <li><NavLink to="home#about" className="page-scroll ">Koch</NavLink></li>
                        <li><NavLink to="home#contact" className="page-scroll ">Kontakt</NavLink></li>
                    </Scrollspy>
                </div>
                {/* /.navbar-collapse  */}
            </div>
        </nav>
    )
}

export default MainHeader