import { Route, Switch, Redirect } from 'react-router-dom'
import React, { useContext, useEffect} from 'react'
import {AppContext} from 'store/AppContext'


import MainHeader from 'components/nav/MainHeader'
import Footer from 'components/layout/Footer'

import NotFound from 'pages/NotFound'

import jquery_run from 'assets/js/layout.jquery'
import Loader from "react-spinners/PuffLoader"

import Page from 'lib/Page'




function App() {

  const {prismicClient, appVisibilty, pageType} = useContext(AppContext)

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    jquery_run()
  }, []);

  useEffect(() => {
  }, [prismicClient, appVisibilty]);

  return (
    <>
      <div className="app-loader" style={{display: appVisibilty ? 'none' : 'block'}}>
        <Loader color="#ffffff" size={120}  />
      </div>
      
      <div className="app-wrapper" data-page-type={pageType} style={{display: !appVisibilty ? 'none' : 'block'}}>
        <MainHeader />
        <Switch>
          <Route path="/" exact><Redirect to="/home" /></Route>
          
          <Route path="/home">
            <Page type="homepage" pageId="YJ2O8RIAACMAtZo4"></Page>
          </Route>
          
          <Route path="/impressum" exact >
            <Page type="page" pageId="YONvwBAAACkAPLwJ"></Page>
          </Route>

          <Route path="/datenschutz" exact >
            <Page type="page" pageId="YON4PhAAACUAPOGj"></Page>
          </Route>
          
          <Route path="*" component={ NotFound } />
        </Switch>
        <Footer />
      </div>
    </>
  );
}

export default App


