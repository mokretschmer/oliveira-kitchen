import React from 'react';
import ReactDOM from 'react-dom';
import App from 'App';
import reportWebVitals from 'reportWebVitals';
import { BrowserRouter } from 'react-router-dom';

import { AppContextProvider } from 'store/AppContext'
import { Helmet, HelmetProvider } from 'react-helmet-async'
import { Provider as BusProvider } from 'react-bus'


import 'assets/css/bootstrap.css'
import 'assets/sass/index.scss'


ReactDOM.render(

  <HelmetProvider>
    <Helmet>
      <script src="js/bootstrap.min.js"></script>
    </Helmet>
    <React.StrictMode>
      <BusProvider>
        <AppContextProvider>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </AppContextProvider>
      </BusProvider>
    </React.StrictMode>
  </HelmetProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
