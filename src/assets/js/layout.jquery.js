import {$} from 'react-jquery-plugin'


const jquery_run = () => {
    $('a.page-scroll').click(function() {
      //("inner")
        if (document.location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && document.location.hostname === this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.first().offset().top - 80
            }, 900);
            return false;
          }
        }
      });

	
    // Show Menu on Book
    $(window).bind('scroll', function() {
        var navHeight = $(window).height() - 600;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });

    // $('body').scrollspy({ 
    //     target: '.navbar-default',
    //     offset: 80
    // });

	// Hide nav on click
  $(".navbar-nav li a").click(function (event) {
    // check if window is small enough so dropdown is created
    var toggle = $(".navbar-toggle").is(":visible");
    if (toggle) {
      $(".navbar-collapse").collapse('hide');
    }
  });
}

export default jquery_run