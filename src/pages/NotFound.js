import React, { useEffect, useContext } from 'react'
import { AppContext } from 'store/AppContext';

const NotFound = (props) => {

    const {setAppVisibilty, setPageType} = useContext(AppContext)

     // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        setPageType('404')
        setAppVisibilty(true)
    }, []);

    return  <React.Fragment >
        <div className="page-wrapper">
            <div className="container">
                <div id="row">
                    <div className="col container-inner">
                        <h1 className="page-title">404</h1>
                        { props.children }
                    </div>
                </div>
                
            </div>
            
        </div>
  </React.Fragment>
}

export default NotFound