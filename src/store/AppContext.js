import React, {useEffect, useState, useCallback} from "react";
import Prismic from '@prismicio/client'
import { EventContextProvider } from 'store/EventContext'


export const AppContext = React.createContext({})

export const AppContextProvider = (props) => {


  const [pageType, setPageType] = useState()
  const [prismicClient, setPrismicClient] = useState(null)
  const [appVisibilty, setAppVisibilty] = useState(false)


  const connectApi = useCallback(async () => {
      const apiEndpoint = 'https://oliveira-kitchen.cdn.prismic.io/api/v2'
      const accessToken = 'MC5ZS0dOUHhJQUFDUUF4cS1U.77-9HUFoXjPvv73vv71ucyouW--_ve-_ve-_vQUhQlAA77-977-9fO-_ve-_ve-_ve-_ve-_ve-_ve-_vQM' // This is where you would add your access token for a Private repository
      const client = Prismic.client(apiEndpoint, { accessToken })
      setPrismicClient(client)
    },
    [],
  );

  useEffect(() => {
    connectApi()
  }, [connectApi]);



  return <>
    <AppContext.Provider value={{
      prismicClient, 
      appVisibilty, 
      setAppVisibilty,
      pageType,
      setPageType
      }}>
      <EventContextProvider>
        { props.children }
      </EventContextProvider>
    </AppContext.Provider></>
  
}
