import React, { useEffect, useContext, useState, useCallback } from "react";
import Prismic from '@prismicio/client'
import { AppContext } from 'store/AppContext'


export const EventContext = React.createContext({})

export const TYPE_EVENT = "event";

export const EventContextProvider = (props) => {


  const { prismicClient } = useContext(AppContext)
  const [events, setEvents] = useState([])


  //fetch data by id
  const fetchEventEntities = useCallback(async () => {
    const response = await prismicClient.query(
      [
        Prismic.Predicates.at('document.type', TYPE_EVENT),
        Prismic.Predicates.dateAfter('my.event.event_date', new Date()),
      ], {
      orderings: '[my.event.event_date desc]',

    }
    )
    if (response) {
      if (response.results && response.results.length) {
        //console.log("set events")
        setEvents(response.results)
        
      } else {
        console.warn(`Could not find any entities with type of "event".`)
      }
    }
  },
    [prismicClient],
  );

  const getLastestEvent = () => {
    return events[0]
  }

  const getEntityById = useCallback(async (id, payload) => {
    if (prismicClient && id && payload.callback) {
      const response = await prismicClient.query(
        Prismic.Predicates.at('document.id', id)
      )
      if (response && response.results) {
        if(response.results.length > 0){
          payload.callback(response.results[0])
        }        
      }
    }
  }, [prismicClient])

  




  useEffect(() => {
    if (prismicClient) {
      fetchEventEntities()
    }

  }, [fetchEventEntities, prismicClient])


  return <>
    <EventContext.Provider value={{ events, getLastestEvent, getEntityById }}>
      {props.children}
    </EventContext.Provider></>

}
