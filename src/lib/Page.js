import React, { Fragment, useEffect, useContext, useRef } from 'react'
import Prismic from '@prismicio/client'
import SlicesView from 'components/layout/SlicesView'
import {AppContext} from 'store/AppContext'
import { useListener } from 'react-bus'

const Page = (props) => {

    const [docData, setDocData] = React.useState(null)
    const {prismicClient, setAppVisibilty, setPageType} = useContext(AppContext)
    const UIpage = useRef()
    

    useEffect(() => {
        if (prismicClient && props.pageId) {
            //fetch data by id
            const fetchData = async () => {
                const response = await prismicClient.query(
                    Prismic.Predicates.at('document.id', props.pageId)
                )
                if (response) {
                    if(response.results.length){
                        setPageType('page')
                        setDocData(response.results[0])
                        setAppVisibilty(true)
                        window.scrollTo(0, 0)
                        
                    }else{
                        console.error(`Could not find page (${props.pageId})`)
                    }
                 
                }
            }
            fetchData()
        }

    }, [props.pageId, prismicClient, setAppVisibilty])


    

    const scrollToForm = React.useCallback(ref =>  {
        const tgt = UIpage.current.querySelector(".slice--type--contact .form-wrapper")
        if(tgt){
            window.scroll({top: tgt.offsetTop, left: 0, behavior: 'smooth' })
        }
      }, [UIpage])

      useListener('SCROLL_TO_FORM', scrollToForm)
    
    let sliceData = null
    if(docData){
        sliceData = docData.data.body
    }

    return (
        <Fragment >
          <div className="page-wrapper" ref={UIpage}>
            { props.children }
            <SlicesView data={sliceData} docData={docData} />
          </div>
        </Fragment>
    )
}

export default Page