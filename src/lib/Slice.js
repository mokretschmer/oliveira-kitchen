import React from 'react'
import $Utils from '../utils/utils'

const Slice = (props) => {

  let sliceOutput = "..."

  if(props.sliceData){

    const slices = props.sliceData.docData.data.body;
    let obj = slices.find(item => item.slice_type === props.sliceData.slice_type);
    const typeIndex= slices.indexOf(obj);
    props.sliceData.uid = `${$Utils.cssSafeStr(props.sliceData.slice_type)}--${props.sliceData.docData.id}--${typeIndex}`;
    


    sliceOutput = (
       <div id={props.id}  data-id={props.sliceData.uid} className={`slice slice--type--${$Utils.cssSafeStr(props.sliceData.slice_type)}`}>
         <a name={`${$Utils.cssSafeStr(props.sliceData.slice_type)}`}></a>
         {props.children}
       </div>
    )
   }

  return (
      <React.Fragment>
        {sliceOutput}
      </React.Fragment>
  )
}

export default Slice