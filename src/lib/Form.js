import React, { useState, useRef, forwardRef, useImperativeHandle  } from "react"
import PropTypes from "prop-types"
import axios from "axios"
import utils from "utils/utils"

/**
 * @component Form
 * @props - { object } -  config
 * 
 **/

const Form = forwardRef((props, ref) => {
  const [mailSent, setMailSent] = useState(false)
  const [error, setError] = useState(null)
  const [errMsg, setErrMsg] = useState([])
  const uiForm = useRef(null);


  const { title, description, successMessage, errorMessage, fields } = props.config;

  useImperativeHandle(ref, () => ({
    resetForm: resetForm
  }));

  const resetForm = () => {
    if(mailSent){
      setMailSent(false)
      setError(false)
      return true
    }
  return false 
  }

  const getFieldsItems = () => {
    const fieldsCollection = {}
    fields.forEach(ele => {
      const fieldItem = uiForm.current.querySelectorAll(`.field-item *[name="${ele.fieldName}"]`)
      if(fieldItem && fieldItem.length){
        fieldsCollection[ele.fieldName] = fieldItem[0]
      }
    })
    return fieldsCollection
  }

  const validateForm = (fieldItems) => {

    let errMsg = []

    fields.forEach(ele => {
      
      if (ele.isRequired) {
        if (!fieldItems[ele.fieldName]) {
          errMsg[ele.fieldName] = "Feld bitte ausfüllen."
          return
        } else {
          if (fieldItems[ele.fieldName].value === '') {
            errMsg[ele.fieldName] = "Feld bitte ausfüllen."
            return
          }
        }
      }

      //email
      if (ele.type === "email") {
        const emailVal = fieldItems[ele.fieldName].value
        let lastAtPos = emailVal.lastIndexOf('@')
        let lastDotPos = emailVal.lastIndexOf('.')
        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && emailVal.indexOf('@@') == -1 && lastDotPos > 2 && (emailVal.length - lastDotPos) > 2)) {
          errMsg[ele.fieldName] = "E-Mail ist nicht korrekt."
        }
        return
      }
    })

    setErrMsg(errMsg)
    return Object.keys(errMsg).length < 1
  }


  /**
  * @function handleFormSubmit
  * @param e { obj } - form event
  * @return void
  */
  const handleFormSubmit = e => {
    e.preventDefault()

    const fieldItems = getFieldsItems()
    const validated = validateForm(fieldItems)
    if (!validated) {
      return false
    }

    const formData = {}

    for (const [key, value] of Object.entries(fieldItems)) {

      //hook select "select-one"
      if("select-one" === value.type){
        formData[key] = value.options[value.selectedIndex].text
      }else{
        formData[key] = value.value
      }
    }


    axios({
      method: "post",
      url: `${props.config.api}`,
      headers: { "content-type": "application/json" },
      data: formData
    })
      .then(result => {
        if (result.data.sent) {
          setMailSent(result.data.sent)
          setError(false)
        } else {
          setError(true)
        }
      })
      .catch(error => setError(error.message));
  };

  return (
    <form ref={uiForm} action="#">
      <div className="row">

        {props.children}

        {fields && !mailSent &&
          fields.map(field => {

            return (
              <div className={`field-item field-item-${utils.cssSafeStr(field.fieldName)} ${field.wrapperClasses}`} key={field.id}>

                {field.type === "text" &&
                  <div className="form-group">
                    {field.labelShown && <label>{field.label}</label>}
                    <input
                      name={field.fieldName}
                      id={field.fieldName}
                      type={field.type}
                      className={field.classes}
                      placeholder={field.placeholder}
                      value={field.name}
                      // onChange={e => handleChange(e, field.fieldName)}
                      required={field.isRequired}
                    />
                    {errMsg[field.fieldName] && <span className="form-err">{errMsg[field.fieldName]}</span>}

                  </div>
                }

                {field.type === "email" &&
                  <div className="form-group">
                    {field.labelShown && <label>{field.label}</label>}
                    <input
                      name={field.fieldName}
                      id={field.fieldName}
                      type={field.type}
                      className={field.classes}
                      placeholder={field.placeholder}
                      value={field.name}
                      // onChange={e => handleChange(e, field.fieldName)}
                      required={field.isRequired}
                    />
                    {errMsg[field.fieldName] && <span className="form-err">{errMsg[field.fieldName]}</span>}

                  </div>
                }

                {field.type === "textarea" &&
                  <div className="form-group">
                    {field.labelShown && <label>{field.label}</label>}
                    <textarea
                      name={field.fieldName}
                      id={field.fieldName}
                      className={field.classes}
                      placeholder={field.placeholder}
                      // onChange={e => handleChange(e, field.fieldName)}
                      value={field.name}
                      required={field.isRequired}
                    />
                    {errMsg[field.fieldName] && <span className="form-err">{errMsg[field.fieldName]}</span>}
                  </div>
                }

                {field.type === "select" &&
                  <div className="form-group">
                    {field.labelShown && <label>{field.label}</label>}
                    <select
                      name={field.fieldName}
                      id={field.fieldName}
                      ref={field.ref}
                      className={field.classes}
                      // onChange={e => handleChange(e, field.fieldName)}
                    >
                      {field.options}
                    </select>
                    {errMsg[field.fieldName] && <span className="form-err">{errMsg[field.fieldName]}</span>}
                  </div>
                }

              </div>
            );
          })}

        {!mailSent &&
          <div className={`form-actions-wrapper ${props.config.formActionsWrapperClasses}`}>
            <button type="submit" className="btn btn-custom btn-lg" onClick={e => handleFormSubmit(e)} value="Submit">Anfrage senden</button>
          </div>
        }

        <div className={`form-validation-message-wrapper ${props.config.formMessageWrapperClasses}`} >
          {mailSent && <div className="form-validation-message form-success">{successMessage}</div>}
          {error && <div className="form-validation-message form-err">{errorMessage}</div>}
        </div>
      </div>
    </form>

  );
})

export default Form;
//propTypes for the component
Form.propTypes = {
  config: PropTypes.object.isRequired
};